package single.responsibility.cohension.ex3;

class User {
    public void CreatePost(Database db, String postMessage) {
        FileUtil fileUtil = new FileUtil();
        try {
            db.Add(postMessage);
        } catch (Exception ex) {
            db.logError("An error occured: ", ex.toString());
            fileUtil.writeAllText("LocalErrors.txt", ex.toString());
        }
    }
}