package dry;

import java.util.ArrayList;
import java.util.List;

public class GoodParent {

    public List<Person> findAllByGender(List<Person> people, Properties.Gender gender) {
        List<Person> results = new ArrayList<>();
        for (Person person : people) {
            if (person.name.startsWith("T")
                    && person.age > 25
                    && person.hairColor == Properties.HairColor.BLACK
                    && person.skinColor == Properties.SkinColor.WHITE) {
                if (person.sex == gender) {
                    results.add(person);
                }
            }

        }

        return results;
    }
}
