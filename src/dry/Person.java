package dry;
public class Person {
    String name;
    int age;
    Properties.Gender sex;
    Properties.HairColor hairColor;
    Properties.SkinColor skinColor;
}