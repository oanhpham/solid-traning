package dry;

public class Properties {
    enum SkinColor {
        BLACK, BROWN, WHITE
    }

    enum HairColor {
        RED, BLACK, YELLOW
    }

    enum Gender {
        FEMALE,
        MALE,
        GAY,
        LES,
    }

}
