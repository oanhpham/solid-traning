package dip;

import single.responsibility.cohension.ex3.Database;

public interface IPost {
     void CreatePost(Database database, String postMessage);
}
