package dip;

import single.responsibility.cohension.ex3.Database;

public class OfficeUser {
    private IPost iPost;

    OfficeUser(IPost iPost) {
        this.iPost = iPost;
    }
    public void publishNewPost() {
        Database db = new Database();
        String postMessage = "example message";
        this.iPost.CreatePost(db, postMessage);
    }
}
