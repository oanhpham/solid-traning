package dip;

public class Main {
    public static void main(String[] args) {
        OfficeUser postGeneral = new OfficeUser(new Post());
        postGeneral.publishNewPost();
        OfficeUser postExternal = new OfficeUser(new PostExternal());
        postExternal.publishNewPost();
    }
}
